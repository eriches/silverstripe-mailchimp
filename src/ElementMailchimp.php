<?php

namespace Hestec\Mailchimp;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\CheckboxField;

class ElementMailchimp extends BaseElement
{

    private static $table_name = 'HestecElementMailchimp';

    private static $singular_name = 'Mailchimp';

    private static $plural_name = 'Mailchimp';

    private static $description = 'Element for Mailchimp signup';

    private static $icon = 'font-icon-p-mail';

    private static $db = [
        'TitleCenter' => 'Boolean',
        'Content' => 'HTMLText',
        'ButtonText' => 'Varchar(50)'
    ];

    private static $has_many = array(
    );

    //private static $inline_editable = false;

    public function getCMSFields()
    {

        $fields = parent::getCMSFields();

        $ContentField = HTMLEditorField::create('Content', "Content");
        $ContentField->setRows(5);

        $TitleCenterField = CheckboxField::create('TitleCenter', "TitleCenter");
        $ButtonTextField = TextField::create('ButtonText', "ButtonText");
        $ButtonTextField->setDescription("Custum button text (optional)");

        $fields->addFieldToTab('Root.Main', $TitleCenterField);
        $fields->addFieldToTab('Root.Main', $ContentField);
        $fields->addFieldToTab('Root.Main', $ButtonTextField);

        return $fields;

    }

    public function getType()
    {
        return 'Mailchimp';
    }
}
