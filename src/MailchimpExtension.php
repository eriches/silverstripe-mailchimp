<?php

namespace Hestec\Mailchimp;

use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\HiddenField;
use DrewM\MailChimp\MailChimp;
use SilverStripe\Core\Config\Config;

class MailchimpExtension extends DataExtension {

    private static $allowed_actions = array(
        'MailchimpAddForm',
        'ElementMailchimpAddForm',
        'ExtraMailchimpAddForm',
        'PopupMailchimpAddForm',
        'MailchimpAddSubscriber'
    );

    public function MailchimpAddForm($button = null, $function = null){

        $functionName = "MailchimpAddForm";
        if (isset($function)){
            $functionName = $function;
        }
        $buttontext = _t("Mailchimp.SUBSCRIBE", "Subscribe");
        if (isset($button) && strlen($button) > 3){
            $buttontext = $button;
        }

        $NameField = TextField::create('Name', false);
        $NameField->setAttribute('placeholder', _t("Mailchimp.NAME", "Name"));
        $EmailField = EmailField::create('Email', false);
        $EmailField->setAttribute('placeholder', _t("Mailchimp.EMAIL", "Email"));
        $LinkField = HiddenField::create('Link', 'Link', $this->owner->Link());

        $Action = FormAction::create('SubmitMailchimpAddForm', $buttontext);

        $fields = FieldList::create(array(
            $NameField,
            $EmailField,
            $LinkField
        ));

        $actions = FieldList::create(
            $Action
        );

        $form = Form::create($this->owner, $functionName, $fields, $actions);
        $form->setTemplate('MailchimpAddForm');

        return $form;

    }

    public function ElementMailchimpAddForm($button = null){

        return $this->MailchimpAddForm($button, __FUNCTION__);

    }

    public function ExtraMailchimpAddForm($button = null){

        return $this->MailchimpAddForm($button, __FUNCTION__);

    }

    public function PopupMailchimpAddForm($button = null){

        return $this->MailchimpAddForm($button, __FUNCTION__);

    }

    public function SubmitMailchimpAddForm($data,$form)
    {

        return $this->owner->redirectBack();

    }

    public function MailchimpAddSubscriber()
    {
        $apikey = Config::inst()->get('Mailchimp', 'apikey');
        $list_id = Config::inst()->get('Mailchimp', 'listid');
        $email = $_GET['email'];
        $name = $_GET['name'];

        $MailChimp = new MailChimp($apikey);

        // Hash het e-mailadres volgens Mailchimp vereisten
        $subscriber_hash = md5(strtolower($email));

        // Controleer de huidige status van de gebruiker
        $response = $MailChimp->get("lists/$list_id/members/$subscriber_hash");

        if (isset($response['status'])) {
            if ($response['status'] == 'subscribed') {
                // Als de gebruiker al geabonneerd is, geef 'Member Exists' terug
                return json_encode(['status' => 400, 'title' => 'Member Exists']);
            } elseif ($response['status'] == 'pending') {
                // Als de gebruiker in 'pending'-status staat, unsubscribe hem eerst
                $result = $MailChimp->put("lists/$list_id/members/$subscriber_hash", [
                    'email_address' => $email,
                    'status'        => 'unsubscribed',
                ]);
            }
        }

        // Voeg de gebruiker opnieuw toe en trigger een nieuwe opt-in e-mail
        $result = $MailChimp->put("lists/$list_id/members/$subscriber_hash", [
            'email_address' => $email,
            'status_if_new' => 'pending',
            'status'        => 'pending',
            'merge_fields'  => ['FNAME' => $name]
        ]);

        return json_encode($result);
    }

}
