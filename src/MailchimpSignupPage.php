<?php

namespace Hestec\Mailchimp;

use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;

class MailchimpSignupPage extends \Page {

    private static $table_name = 'HestecMailchimpSignupPage';

    private static $db = array(
        'ConfirmedTitle' => 'Varchar(255)',
        'ConfirmedText' => 'HTMLText',
        'UnsubscribedTitle' => 'Varchar(255)',
        'UnsubscribedText' => 'HTMLText',
        'SignupTitle' => 'Varchar(255)',
        'SignupText' => 'HTMLText',
        'SubscribedCookie' => 'Boolean'
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $ConfirmedTitleField = TextField::create('ConfirmedTitle', "ConfirmedTitle");
        $ConfirmedTextField = HTMLEditorField::create('ConfirmedText', "ConfirmedText");
        $UnsubscribedTitleField = TextField::create('UnsubscribedTitle', "UnsubscribedTitle");
        $UnsubscribedTextField = HTMLEditorField::create('UnsubscribedText', "UnsubscribedText");
        $SignupTitleField = TextField::create('SignupTitle', "SignupTitle");
        $SignupTextField = HTMLEditorField::create('SignupText', "SignupText");
        $SubscribedCookieField = CheckboxField::create('SubscribedCookie', "SubscribedCookie");
        $SubscribedCookieField->setDescription("Zet een cookie met de naam McSubscribed om de mailchimp popup niet meer te tonen.");

        $fields->addFieldsToTab(
            'Root.Signup', [
                $SignupTitleField,
                $SignupTextField
            ]
        );

        $fields->addFieldsToTab(
            'Root.Confirmed', [
                $SubscribedCookieField,
                $ConfirmedTitleField,
                $ConfirmedTextField
            ]
        );

        $fields->addFieldsToTab(
            'Root.Unsubscribed', [
                $UnsubscribedTitleField,
                $UnsubscribedTextField
            ]
        );

        return $fields;
    }

}
