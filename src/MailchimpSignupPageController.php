<?php

namespace Hestec\Mailchimp;

use SilverStripe\Control\Cookie;

class MailchimpSignupPageController extends \PageController {

    private static $allowed_actions = array (
        'confirmed',
        'unsubscribed'
    );

    public function confirmed(){

        $this->getRequest()->getSession()->set('mcConfirmed', 1);

        if ($this->SubscribedCookie == true){

            Cookie::set('McSubscribed', 1, $expiry = 90);

        }

        return $this->redirect($this->Link());

    }

    public function unsubscribed(){

        $this->getRequest()->getSession()->set('mcUnsubscribed', 1);

        return $this->redirect($this->Link());

    }

    public function isConfirmed(){

        if ($this->getRequest()->getSession()->get('mcConfirmed') === 1){

            $this->getRequest()->getSession()->clear('mcConfirmed');

            return true;

        }
        return false;

    }

    public function isUnsubscribed(){

        if ($this->getRequest()->getSession()->get('mcUnsubscribed') === 1){

            $this->getRequest()->getSession()->clear('mcUnsubscribed');

            return true;

        }
        return false;

    }

}
