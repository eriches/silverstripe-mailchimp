<% include Header %>

<main role="main">
    <div class="container pb-5 pt-5">
        <% include ShareButtons %>

        <% if $isConfirmed %>
            <h1>$ConfirmedTitle</h1>
            $ConfirmedText
        <% else %>

        <h1>$Title</h1>
        $Content

        <h2>$SignupTitle</h2>
        <div class="row">
            <div class="col-12 col-md-6">
                $SignupText
                <p class="d-none d-lg-block"><small>Wij sturen maximaal 6 keer per jaar een nieuwsbrief. Wij spammen niet, delen je gegevens nooit met derden en je kunt je op elk moment weer uitschrijven.</small></p>
            </div>
            <div class="col-12 col-md-6">
                <div id="extra-mc-subscribe">
                    <span class="mc-message"></span>
                    <div id="extra-mc-form">$ExtraMailchimpAddForm</div>
                </div>
            </div>
        </div>
        <p class="d-lg-none mt-3"><small>Wij sturen maximaal 6 keer per jaar een nieuwsbrief. Wij spammen niet, delen je gegevens nooit met derden en je kunt je op elk moment weer uitschrijven.</small></p>

        <% end_if %>

    </div>



    $ElementalArea

</main>

<% include Footer %>